import { NgModule, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lt',
})
export class LessThanPipe implements PipeTransform {
  transform(leftSide: number | string, rightSide: number | string): boolean {
    return +leftSide < +rightSide;
  }
}

@Pipe({
  name: 'gt',
})
export class GreaterThanPipe implements PipeTransform {
  transform(leftSide: number | string, rightSide: number | string): boolean {
    return +leftSide > +rightSide;
  }
}

@NgModule({
  declarations: [
    LessThanPipe,
    GreaterThanPipe,
  ],
  exports: [
    LessThanPipe,
    GreaterThanPipe,
  ],
})
export class ComparisonsModule {}
