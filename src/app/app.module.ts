import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql.module';

import { ComparisonsModule } from './comparisons/comparisons.module';
import { ExchangeRatesComponent } from './exchange-rates.component';
import { InCachePipe } from './in-cache.pipe';

@NgModule({
  declarations: [
    AppComponent,

    ExchangeRatesComponent,
    InCachePipe,
  ],
  exports: [
    ExchangeRatesComponent,
    InCachePipe,
  ],
  imports: [
    BrowserModule,
    GraphQLModule,
    HttpClientModule,
    ComparisonsModule,
    MatSelectModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
