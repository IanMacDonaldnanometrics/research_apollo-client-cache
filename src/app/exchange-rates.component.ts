import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult, WatchQueryFetchPolicy } from 'apollo-client';
import { combineLatest, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, filter, map, mergeMap, startWith, take, tap } from 'rxjs/operators';

import { ExchangeRate } from './exchange-rate';

@Component({
  selector: 'app-exchange-rates',
  template: `
    <div style="display: flex">
      <div class="currency" style="flex-grow: 1">{{ loading ? 'Loading...' : selected?.currency }} {{ loading ? '' : selected?.name }}</div>

      <div style="flex-grow: 0">
        <mat-form-field>
          <mat-label>Cache Policy</mat-label>
          <mat-select (selectionChange)="fetchPolicy$.next($event.value)">
            <mat-option value="network-only">Network only</mat-option>

            <mat-option value='cache-first'>Cache First</mat-option>
            <mat-option value='cache-only'>Cache Only</mat-option>
            <mat-option value='no-cache'>No Cache</mat-option>
            <mat-option value='standby'>Standby</mat-option>
            <mat-option value='cache-and-network'>Cache and Network</mat-option>

            <mat-option [value]="undefined">Default</mat-option>
          </mat-select>
        </mat-form-field>
      </div>
    </div>

    <div *ngIf="loading" class="loading">
      No data.
    </div>
    <div *ngIf="errors">
      Error :(
      <div *ngFor="let error of errors">
        <p>{{error}}</p>
      </div>
    </div>
    <div *ngIf="rates && !loading">
      <div class="rates">
        <div *ngFor="let rate of rates" class="currency-item"
          (click)="selected$.next(rate)"
          [class.selected]="selected?.currency === rate.currency"
          [class.cached]="rate.currency | incache"
          >
          <div class="rate">{{rate.currency}}</div>
          <div class="delta"
            [class.positive]="rate.rate | gt:1.0"
            [class.negative]="rate.rate | lt:1.0">
            {{ rate.rate | currency:selected?.currency:'symbol':'1.2-2' }}
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./style.css'],
})
export class ExchangeRatesComponent implements OnInit {
  rates: any;
  loading: boolean = true;
  errors: any;
  fetchPolicy$: Subject<WatchQueryFetchPolicy | undefined> = new Subject<WatchQueryFetchPolicy | undefined>();

  selected: ExchangeRate | undefined;
  selected$: Subject<ExchangeRate> = new Subject<ExchangeRate>();

  constructor(private apollo: Apollo) {
  }

  ngOnInit(): void {
    combineLatest(
      this.fetchPolicy$.pipe(
        startWith(undefined),
        distinctUntilChanged((x: WatchQueryFetchPolicy | undefined, y: WatchQueryFetchPolicy | undefined) => x === y),
      ),
      this.selected$.pipe(
        map((rate: ExchangeRate) => rate.currency),
        startWith('CAD'),
        distinctUntilChanged((x: string, y: string) => x === y),
      ),
    ).pipe(
      tap(() => this.loading = true),
      map(([fetchPolicy, currency]: [WatchQueryFetchPolicy | undefined, string]) =>
        [fetchPolicy, currency, ExchangeRate.getQuery(currency)]),
      mergeMap(([fetchPolicy, currency, query]: [WatchQueryFetchPolicy | undefined, string, any]) =>
        this.apollo.watchQuery<{rates: ExchangeRate[]}>({
          query,
          fetchPolicy,
        })
        .valueChanges.pipe(
          take(1),
          catchError(() => of(undefined)),
          filter((x?: ApolloQueryResult<{rates: ExchangeRate[]}>) => !!x),
          tap((result: ApolloQueryResult<{rates: ExchangeRate[]}>) => {
            if (!!result && !!result.data && !!result.data.rates) {
              const match: ExchangeRate = result.data.rates.find((rate: ExchangeRate) => rate.currency === currency);
              if (!!match) {
                this.selected = match;
              }
            }
          }),
        ),
      ),
    ).subscribe((result: ApolloQueryResult<{rates: ExchangeRate[]}>) => {
      this.rates = result.data && result.data.rates;
      this.loading = false;
      this.errors = result.errors;
    });
  }
}
