import { Pipe, PipeTransform } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ExchangeRate } from './exchange-rate';

@Pipe({
  name: 'incache',
})
export class InCachePipe implements PipeTransform {
  constructor(private apollo: Apollo) {}

  transform(currency: string): boolean {
    try {
      return !!this.apollo.getClient().readQuery({query: ExchangeRate.getQuery(currency)});
    } catch (e) {
      return false;
    }
  }
}
