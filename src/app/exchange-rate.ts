import gql from 'graphql-tag';

export interface ExchangeRate {
  currency: string;
  rate: number;
  name: string;
}

export namespace ExchangeRate {
  export function getQuery(currency: string): any {
    return gql`
      {
        rates(currency: "${currency}") {
          name
          currency
          rate
        }
      }
      `;
  }
}
