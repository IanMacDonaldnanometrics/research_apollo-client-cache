export interface KeyedMap<T> {
  [key: string]: T;
}

export function arrayToMap<T>(items: T[], lookup: string): KeyedMap<T> {
  return items.reduce((collector: KeyedMap<T>, item: T) => {
      collector[item[lookup]] = item;
      return collector;
  }, {});
}
